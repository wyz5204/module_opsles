//
// Created by Administrator on 2021/5/11.
//

#ifndef TESTNDK_LOG_H
#define TESTNDK_LOG_H

#include <android/log.h>

#define LOG_ENABLE

#define LOG_TAG "ATOM_NDK"//这是tag的名字

#ifdef LOG_ENABLE

#undef LOG
// ... 代表可变参数 ， 对应使用__VA_ARGS__进行调用
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGF(...)  __android_log_print(ANDROID_LOG_FATAL,LOG_TAG,__VA_ARGS__)
#else
#define LOGD(...)
#define LOGI(...)
#define LOGW(...)
#define LOGE(...)
#define LOGF(...)

#endif
#endif //TESTNDK_LOG_H
